**Purpose**
- The purpose of this model is to detect the Cardio-vascular disease, AF with using the ECG data.
- Here, I tried to construct the ResNet model as it has shown in the previous published paper in the following.

[https://drive.google.com/file/d/1yyi3VmTDPcO5rTl95UGzGiHaly8mOFge/view?usp=sharing](https://drive.google.com/file/d/1yyi3VmTDPcO5rTl95UGzGiHaly8mOFge/view?usp=sharing)

[https://drive.google.com/file/d/1yyi3VmTDPcO5rTl95UGzGiHaly8mOFge/view?usp=sharing](https://drive.google.com/file/d/11wu76xNtLOpIhqZmBwTfhZJtre3mgqUF/view?usp=sharing)

**Why it shares?**
- Now I will share the source code and would like to get the peer-code-review in order to understand the reason why my model doensn't work.
- The check point is the ResNet model with keras + TF. (This is the first time to construct the DL model...)
- I've already uploaded the sample data here, but if it's not enough, I uploaded another 1000 data in the following temporal folder.

[https://drive.google.com/drive/folders/18e8dAPo6TuhwtiAfhllEkqmvRydCI0AU?usp=sharing](https://drive.google.com/drive/folders/18e8dAPo6TuhwtiAfhllEkqmvRydCI0AU?usp=sharing)

*If you have any question of this project, please feel free to ask me a question.*